# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the fr.romainvigier.MetadataCleaner package.
# liimee <alt3753.7@gmail.com>, 2021.
# Reza Almanda <rezaalmanda27@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: fr.romainvigier.MetadataCleaner\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-30 18:50+0200\n"
"PO-Revision-Date: 2021-08-13 10:21+0000\n"
"Last-Translator: Reza Almanda <rezaalmanda27@gmail.com>\n"
"Language-Team: Indonesian <https://hosted.weblate.org/projects/metadata-"
"cleaner/application/id/>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.8-dev\n"

#: data/fr.romainvigier.MetadataCleaner.desktop.in:6
#: data/fr.romainvigier.MetadataCleaner.desktop.in:7
#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:9
#: data/ui/AboutDialog.ui:66 metadatacleaner/app.py:38
msgid "Metadata Cleaner"
msgstr "Pembersih Metadata"

#: data/fr.romainvigier.MetadataCleaner.desktop.in:8
msgid "Clean metadata from your files"
msgstr "Bersihkan metadata dari berkas-berkas Anda"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/fr.romainvigier.MetadataCleaner.desktop.in:16
msgid "Metadata;Remover;Cleaner;"
msgstr "Metadata;Penghapus;Pembersih;"

#: data/fr.romainvigier.MetadataCleaner.gschema.xml:10
msgid "Clean without warning"
msgstr "Bersihkan tanpa peringatan"

#: data/fr.romainvigier.MetadataCleaner.gschema.xml:11
msgid "Clean the files without showing the warning dialog"
msgstr "Bersihkan file tanpa menampilkan dialog peringatan"

#: data/fr.romainvigier.MetadataCleaner.gschema.xml:15
#: data/ui/SettingsButton.ui:27
msgid "Lightweight cleaning"
msgstr "Pembersihan ringan"

#: data/fr.romainvigier.MetadataCleaner.gschema.xml:16
msgid "Don't make destructive changes to files but may leave some metadata"
msgstr ""

#: data/fr.romainvigier.MetadataCleaner.gschema.xml:20
msgid "Window width"
msgstr "Lebar jendela"

#: data/fr.romainvigier.MetadataCleaner.gschema.xml:21
msgid "Saved width of the window"
msgstr "Simpan lebar dari jendela"

#: data/fr.romainvigier.MetadataCleaner.gschema.xml:25
msgid "Window height"
msgstr "Tinggi jendela"

#: data/fr.romainvigier.MetadataCleaner.gschema.xml:26
msgid "Saved height of the window"
msgstr "Simpan tinggi dari jendela"

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:10
msgid "View and clean metadata in files"
msgstr "Lihat dan bersihkan metadata pada berkas-berkas"

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:26
#, fuzzy
#| msgid ""
#| "Metadata within a file can tell a lot about you. Cameras record data "
#| "about when a picture was taken and what camera was used. Office "
#| "applications automatically add author and company information to "
#| "documents and spreadsheets. Maybe you don't want to disclose those "
#| "informations."
msgid ""
"Metadata within a file can tell a lot about you. Cameras record data about "
"when and where a picture was taken and which camera was used. Office "
"aplications automatically add author and company information to documents "
"and spreadsheets. This is sensitive information and you may not want to "
"disclose it."
msgstr ""
"Metadata dalam file dapat mengetahui banyak tentang Anda. Kamera merekam "
"data tentang kapan gambar diambil dan kamera apa yang digunakan. Aplikasi "
"Office secara otomatis menambahkan penulis dan informasi perusahaan ke "
"dokumen dan spreadsheet. Mungkin Anda tidak ingin mengungkapkan informasi-"
"informasi tersebut."

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:27
#, fuzzy
#| msgid ""
#| "This tool allows you to view metadata in your files and to get rid of "
#| "them, as much as possible."
msgid ""
"This tool allows you to view metadata in your files and to get rid of it, as "
"much as possible."
msgstr ""
"Alat ini membantu Anda untuk melihat metadata pada berkas-berkas Anda dan "
"membersihkannya, sebanyak mungkin."

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:74
msgid "Improved adaptive user interface"
msgstr "Tampilan antarmuka adaptif yang ditingkatkan"

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:75
msgid "New help pages"
msgstr "Halaman bantuan baru"

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:76
#, fuzzy
#| msgid "One-click cleaning, no need to clean then save anymore"
msgid "One-click cleaning, no need to save after cleaning"
msgstr "Pembersihan satu-klik, tidak perlu bersihkan lalu simpan"

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:77
#, fuzzy
#| msgid "Lightweight cleaning option is now saved"
msgid "Persistent lightweight cleaning option"
msgstr "Opsi mode ringan sekarang disimpan"

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:84
#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:91
msgid "New translations"
msgstr "Terjemahan baru"

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:98
msgid "Croatian translation (contributed by Milo Ivir)"
msgstr "Terjemahan Bahasa Kroasia (dikontribusikan oleh Milo Ivir)"

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:99
msgid "Portuguese (Brazil) translation (contributed by Gustavo Costa)"
msgstr ""
"Terjemahan Bahasa Portugis (Brazil) (dikontribusikan oleh Gustavo Costa)"

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:106
msgid "New in v1.0.4:"
msgstr "Apa yang baru di v1.0.4:"

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:108
msgid "Turkish translation (contributed by Oğuz Ersen)"
msgstr "Terjemahan bahasa Turki (oleh Oğuz Ersen)"

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:115
msgid "New in v1.0.2:"
msgstr "Baru di v1.0.2:"

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:117
msgid "Spanish translation (contributed by Óscar Fernández Díaz)"
msgstr "Terjemahan Bahasa Spanyol (disumbangkan oleh Óscar Fernández Díaz)"

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:118
msgid "Swedish translation (contributed by Åke Engelbrektson)"
msgstr "Terjemahan Swedia (disumbangkan oleh Åke Engelbrektson)"

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:120
msgid "Bug fixes:"
msgstr "Perbaikan bug:"

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:122
msgid "Files with uppercase extension can now be added"
msgstr "Berkas dengan ekstensi huruf besar kini bisa ditambahkan"

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:128
msgid "New in v1.0.1:"
msgstr "Baru di v1.0.1:"

#: data/fr.romainvigier.MetadataCleaner.metainfo.xml:130
msgid "German translation (contributed by lux)"
msgstr "Terjemahan Bahasa Jerman (disumbangkan oleh lux)"

#: data/gtk/help-overlay.ui:15
msgid "Files"
msgstr "Berkas-berkas"

#: data/gtk/help-overlay.ui:18 data/ui/AddFilesButton.ui:8
msgid "Add files"
msgstr "Tambah berkas"

#: data/gtk/help-overlay.ui:24
msgid "Clean metadata"
msgstr "Bersihkan metadata"

#: data/gtk/help-overlay.ui:30
msgid "Clear all files from window"
msgstr "Hapus semua berkas dari jendela"

#: data/gtk/help-overlay.ui:38
msgid "General"
msgstr "Umum"

#: data/gtk/help-overlay.ui:41
msgid "New window"
msgstr "Jendela baru"

#: data/gtk/help-overlay.ui:47
msgid "Close window"
msgstr "Tutup jendela"

#: data/gtk/help-overlay.ui:53
msgid "Quit"
msgstr "Keluar"

#: data/gtk/help-overlay.ui:59
msgid "Keyboard shortcuts"
msgstr "Pintasan papan ketik"

#: data/gtk/help-overlay.ui:65
msgid "Help"
msgstr "Bantuan"

#: data/ui/AboutDialog.ui:87
msgid "Chat on Matrix"
msgstr "Mengobrol di Matrix"

#: data/ui/AboutDialog.ui:97
msgid "View the code on GitLab"
msgstr "Lihat kodenya di GitLab"

#: data/ui/AboutDialog.ui:107
msgid "Translate on Weblate"
msgstr "Terjemahkan di Weblate"

#: data/ui/AboutDialog.ui:117
msgid "Support us on Liberapay"
msgstr "Dukung kami di Liberapay"

#: data/ui/AboutDialog.ui:147
msgid "Code"
msgstr "Kode"

#: data/ui/AboutDialog.ui:153
msgid "Artwork"
msgstr "Ilustrasi"

#: data/ui/AboutDialog.ui:159
msgid "Documentation"
msgstr "Dokumentasi"

#: data/ui/AboutDialog.ui:165
msgid "Translation"
msgstr "Terjemahan"

#: data/ui/AboutDialog.ui:170
msgid ""
"This program uses <a href=\"https://0xacab.org/jvoisin/mat2\">mat2</a> to "
"parse and clean the metadata. Show them some love!"
msgstr ""
"Program ini menggunakan <a href=\"https://0xacab.org/jvoisin/mat2\">mat2</a> "
"untuk membersihkan metadata. Tunjukkan pada mereka cinta!"

#: data/ui/AboutDialog.ui:183
msgid ""
"The source code of this program is released under the terms of the <a href="
"\"https://www.gnu.org/licenses/gpl-3.0.html\">GNU GPL 3.0 or later</a>. The "
"original artwork and translations are released under the terms of the <a "
"href=\"https://creativecommons.org/licenses/by-sa/4.0/\">CC BY-SA 4.0</a>."
msgstr ""
"Kode sumber dari program ini dirilis berdasarkan ketentuan <a href=\"https://"
"www.gnu.org/licenses/gpl-3.0.html\">GNU GPL 3.0 atau yang lebih baru</a>. "
"Karya seni dan terjemahan asli dirilis di bawah ketentuan <a href=\"https://"
"creativecommons.org/licenses/by-sa/4.0/\">CC BY-SA 4.0</a>."

#: data/ui/CleanMetadataButton.ui:9
msgid "_Clean"
msgstr "_Bersihkan"

#: data/ui/CleaningWarningDialog.ui:9
msgid "Make sure you backed up your files!"
msgstr "Pastikan Anda sudah mencadangkan berkas Anda!"

#: data/ui/CleaningWarningDialog.ui:10
msgid "Once the files are cleaned, there's no going back."
msgstr "Setelah berkas dibersihkan, tidak bisa kembali semula lagi."

#: data/ui/CleaningWarningDialog.ui:17
msgid "Don't tell me again"
msgstr "Jangan beritahu lagi"

#: data/ui/CleaningWarningDialog.ui:25
msgid "Cancel"
msgstr "Batal"

#: data/ui/CleaningWarningDialog.ui:30
msgid "Clean"
msgstr "Bersihkan"

#: data/ui/EmptyView.ui:13
msgid "Clean your traces."
msgstr "Hapus jejak Anda."

#: data/ui/EmptyView.ui:27
msgid "Learn more about metadata and the cleaning process limitations."
msgstr "Pelajari lebih lanjut tentang metadata dan batasan proses pembersihan."

#: data/ui/FileRow.ui:12
msgid "Remove file from list"
msgstr "Hapus berkas dari daftar"

#: data/ui/FileRow.ui:93
msgid "Warning"
msgstr "Peringatan"

#: data/ui/FileRow.ui:111
msgid "Error"
msgstr "Galat"

#: data/ui/FileRow.ui:151
msgid "Cleaned"
msgstr "Dibersihkan"

#: data/ui/MenuButton.ui:10
msgid "_New window"
msgstr "_Jendela baru"

#: data/ui/MenuButton.ui:14
msgid "_Clear window"
msgstr "_Bersihkan jendela"

#: data/ui/MenuButton.ui:20
msgid "_Help"
msgstr "Bant_uan"

#: data/ui/MenuButton.ui:25
msgid "_Keyboard shortcuts"
msgstr "_Pintasan papan ketik"

#: data/ui/MenuButton.ui:29
msgid "_About Metadata Cleaner"
msgstr "_Tentang Metadata Cleaner"

#: data/ui/SettingsButton.ui:9
msgid "Cleaning settings"
msgstr "Pengaturan pembersihan"

#: data/ui/SettingsButton.ui:32
msgid "Learn more about the lightweight cleaning"
msgstr "Pelajari lebih lanjut tentang pembersihan ringan"

#: data/ui/Window.ui:72
msgid "Details"
msgstr "Keterangan"

#: data/ui/Window.ui:78
msgid "Close"
msgstr "Tutup"

#. Translators: Replace `translator-credits` by your name, and optionally your email address between angle brackets (Example: `Name <mail@example.org>`). If names are already present, do not remove them and add yours on a new line.
#: data/ui/Window.ui:98
msgid "translator-credits"
msgstr ""
"liimee\n"
"rezaalmanda"

#: data/ui/Window.ui:107
msgid "Choose files to clean"
msgstr "Pilih berkas yang ingin dibersihkan"

#: metadatacleaner/modules/file.py:223
msgid "Something bad happened during the cleaning, cleaned file not found"
msgstr ""
"Sesuatu buruk terjadi ketika pembersihan, berkas yang dibersihkan tidak "
"ditemukan"

#: metadatacleaner/ui/detailsview.py:58
msgid "The file has been cleaned."
msgstr "Berkas telah dibersihkan."

#: metadatacleaner/ui/detailsview.py:69
msgid "Unable to read the file."
msgstr "Tidak dapat membaca berkas."

#: metadatacleaner/ui/detailsview.py:70
msgid "File type not supported."
msgstr "Jenis berkas tidak didukung."

#: metadatacleaner/ui/detailsview.py:72
msgid "Unable to check metadata."
msgstr "Gagal saat memeriksa metadata."

#: metadatacleaner/ui/detailsview.py:74
msgid "No known metadata, the file will be cleaned to be sure."
msgstr ""
"Tidak ada metadata yang ditemukan. Berkas akan tetap dibersihkan, lebih baik "
"aman daripada menyesal."

#: metadatacleaner/ui/detailsview.py:76
msgid "Unable to remove metadata."
msgstr "Tidak dapat menghapus metadata."

#: metadatacleaner/ui/filechooserdialog.py:24
msgid "All supported files"
msgstr "Semua berkas yang didukung"

#: metadatacleaner/ui/statusindicator.py:35
msgid "Processing file {}/{}"
msgstr "Memproses berkas {}/{}"

#~ msgid ""
#~ "Trade some metadata's presence in exchange of the guarantee that the data "
#~ "won't be modified"
#~ msgstr ""
#~ "Perdagangkan beberapa metadata sebagai ganti jaminan bahwa data tidak "
#~ "akan dimodifikasi"

#~ msgid "Warn before saving cleaned files"
#~ msgstr "Peringati sebelum menyimpan berkas-berkas yang telah dibersihkan"

#~ msgid "Show the warning dialog before saving the cleaned files"
#~ msgstr ""
#~ "Tampilkan dialog peringatan sebelum menyimpan berkas-berkas yang telah "
#~ "dibersihkan"

#~ msgid "Romain Vigier"
#~ msgstr "Romain Vigier"

#~ msgid "Note about metadata and privacy"
#~ msgstr "Catatan tentang metadata dan privasi"

#~ msgid "Metadata"
#~ msgstr "Metadata"

#~ msgid "Note about _removing metadata"
#~ msgstr "Catatan mengenai _menghapus metadata"

#~ msgid "Metadata details"
#~ msgstr "Rincian metadata"

#~ msgid "_Save"
#~ msgstr "_Simpan"

#~ msgctxt "shortcut window"
#~ msgid "Add files"
#~ msgstr "Tambah berkas"

#~ msgid "Done!"
#~ msgstr "Selesai!"

#~ msgid "Initializing…"
#~ msgstr "Memulai…"

#~ msgid "File type supported."
#~ msgstr "Jenis berkas didukung."

#~ msgid "Removing metadata…"
#~ msgstr "Menghapus metadata…"

#~ msgid "Error while removing metadata:"
#~ msgstr "Galat saat menghapus metadata:"

#~ msgid "Saving the cleaned file…"
#~ msgstr "Menyimpan berkas yang dibersihkan…"

#~ msgid "Error while saving the file:"
#~ msgstr "Galat saat menyimpan berkas:"

#~ msgid "The cleaned file has been saved."
#~ msgstr "Berkas yang dibersihkan telah disimpan."

#~ msgid "{filename}:"
#~ msgstr "{filename}:"
