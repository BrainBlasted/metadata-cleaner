<!--
SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# Changelog


## [1.0.8] - 2021-08-10

### Added
- New Dutch translation (contributed by @Vistaus)
- New Italian translation (contributed by @albanobattistella)
- Initial work on Sinhala translation (contributed by HelaBasa)

### Changed
- Updated Indonesian translation (contributed by @t7260)


## [1.0.7] - 2021-07-05

### Added
- New Lithuanian translation (contributed by Gediminas Murauskas)
- Initial work on Finnish translation (contributed by @artnay)
- Initial work on Indonesian translation (contributed by @kingu)
- Initial work on Norwegian Bokmål translation (contributed by @t7260 and Reza Almanda)


## [1.0.6] - 2021-05-04

### Added

- New Croatian translation (contributed by @milotype)
- New Portuguese (Brazil) translation (contributed by @xfgusta)


## [1.0.5] - 2021-04-13

### Fixed
- Wrong accelerator in the shortcuts window


## [1.0.4] - 2021-04-02

### Added
- New Turkish translation (contributed by @ersen)


## [1.0.3] - 2021-02-17

### Changed
- Updated Spanish translation


## [1.0.2] - 2021-01-15

### Added
- New Spanish translation (contributed by @oscfdezdz)
- New Swedish translation (contributed by @eson)

### Fixed
- Files with uppercase extension can't be added


## [1.0.1] - 2020-12-28

### Added
- New German translation (contributed by @lux)


## [1.0.0] - 2020-12-10

First release! 🎉
